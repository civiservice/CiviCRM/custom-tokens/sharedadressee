<?php

/**
 * Token zur Anzeige des Adressaten-Feldes des Master-Kontaktes 
 * für die primäre Anschrift, wenn diese eine geteilte Anschrift ist
 * (mit und ohne anschließenden Umbruch)
 * 
 * Token for displaying the addressee field of the master contact 
 * for the primary address if this is a split address 
 * (with and without subsequent line break).
 * 
 * Copyright (C) 2022 civiservice.de GmbH <info@civiservice.de>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

function sharedaddress_addressee($cid, $newLine) {

    // Get primary address and determine master ID for it
    $sharedaddress = civicrm_api4('Address', 'get', [
      'select' => [
        'contact_id.display_name', 
        'master_id',
      ],
      'where' => [
        ['contact_id', '=', $cid], 
        ['is_primary', '=', TRUE],
      ],
    ]);
    if(empty($sharedaddress[0]["master_id"])) return '';

    // Get CiviCRM ID and address of the master contact
    $contact_addressee = civicrm_api4('Address', 'get', [
      'select' => [
        'id', 
        'contact_id.addressee_display', 
        'master_id',
      ],
      'where' => [
        ['id', '=', $sharedaddress[0]["master_id"]],
      ],
    ]);

    // Add a new line, if the address is not empty e.g. there is shared address
    $address = $contact_addressee[0]['contact_id.addressee_display'];
    if(!empty($address && $newLine)) 
    {
      $address = $address . '<br>';
    }

    return $address;
}

// Declare tokens and fill in values
function sharedaddressee_civitoken_declare($token){
    return array(
        $token . '.sharedaddress_addressee' => ts('Adressat geteilte Anschrift (mit Umbruch)'),
        $token . '.sharedaddress_addressee_nobreak' => ts('Adressat geteilte Anschrift (ohne Umbruch)'),
    );
}

function sharedaddressee_civitoken_get($cid, &$value) {
    $value['sharedaddressee.sharedaddress_addressee'] = sharedaddress_addressee($cid, true);
    $value['sharedaddressee.sharedaddress_addressee_nobreak'] = sharedaddress_addressee($cid, false);
}
