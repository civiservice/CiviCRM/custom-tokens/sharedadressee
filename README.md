# Shared Adressee

This provides simple additional tokens for [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) displaying the addressee field of the master contact for the primary address if this is a split address (with and without subsequent line break)
## Installation
* Install [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) extension if not yet done
* Clone this repo into the subfolder *tokens* in your Custom PHP directory (or save them manually in this directory) – e.g. *uploads/civicrm/custom_php/tokens*
* Activate added tokens in *Administer | Communications | Endabled Tokens

## Usage
Use <code>{sharedaddressee.sharedaddress_addressee}</code> and <code>{sharedaddressee.sharedaddress_addressee_nobreak}</code> to print a contact's "remote" adressee for the primary address if this address is a shared address. 

Example adding adding shared adressee as an extra line to the default adress block token coming with Fuzion:
<code>{sharedaddressee.sharedaddress_addressee}{address.address_block}</code>

## License
The extension is licensed under [AGPL-3.0](LICENSE.txt).